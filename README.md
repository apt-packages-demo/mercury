# mercury

General-purpose logic/functional programming language. http://www.mercurylang.org

## Official documentation
* [Tutorial at official GitHub](https://github.com/Mercury-Language/mercury/wiki/Tutorial)

## Unofficial documentation
* [*Learn X in Y minutes, Where X=mercury*](https://learnxinyminutes.com/docs/mercury)
* [WikipediA](https://en.m.wikipedia.org/wiki/Mercury_(programming_language))
* [repology.org](https://repology.org/project/mercury)

## Results
* doc-debian-testing: [master](https://gitlab.com/apt-packages-demo/mercury/-/jobs/artifacts/master/file/tree.html?job=doc-debian-testing)

## Neighbour languages
* Prolog (NU-Prolog and SICStus Prolog)
* ML
* Haskell
* (Fortran)

## Projects using neighbour languages
* ML
  * Standard ML
    * MLton
      * [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj)
    * Poly/ML
      * [pacman-packages-demo/polyml](https://gitlab.com/pacman-packages-demo/polyml)
    * SML/NJ
      * [pacman-packages-demo/smlnj](https://gitlab.com/pacman-packages-demo/smlnj)
  * OCaml
    * [pacman-packages-demo/ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
    * [ocaml-packages-demo](https://gitlab.com/ocaml-packages-demo)
...